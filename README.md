# GitLab Runners

This repository contains scripts to launch GitLab Runners for your group.
Once configured, GitLab runners are called by the GitLab server to perform computations (package, building, test, etc) on your code, after each commit. Runners are not executed on the server but on remote computer accessible by the network.
This repository contains various scripts to initialise a GitLab Runner, to register it for your group with the GitLab server, and to launch it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine

### Prerequisites

You need the registration token for your group from the GitLab server.
For this go to your group page, **settings**, **CI/CD** and **Runners settings**. For the LBMC group, this corresponds to the following url :

[https://gitlab.biologie.ens-lyon.fr/groups/LBMC/-/settings/ci_cd](https://gitlab.biologie.ens-lyon.fr/groups/LBMC/-/settings/ci_cd)

Cloning this repository:

```sh
git clone gitlab_lbmc:LBMC/gitlab_runner.git
```

### Installing

First, we need to build a custom container of the gitlab-runner with the following command.

```sh
sudo mkdir -p /srv/gitlab-runner/config
gitlab_runner/docker_init.sh
```

This will build the `gitlab_runner` container with the certificate of the [gitlab.biologie.ens-lyon.fr](https://gitlab.biologie.ens-lyon.fr/) website installed.

This container will be the process that register and manage the GitLab runners on your computer.

To run the runners safely themselves, we are going to instantiate new containers, for each runner.
For example, to have a runner for `R` development `r_dev/Dockerfile`. Those containers must be available from the [Docker store](https://store.docker.com/community/images/lmodolo/r_dev/tags).

The `gitlab_runner` container will be able to pull them and launch them (within the `r_dev:3.5.1` container for the `r_dev` example)


## Registering a Runner

To register a runner you need to use the `*_runner_register.sh` scripts.

For example, for a `r_dev:3.5.1` runner register it with the following command, where `<group_token>` is the token of your group :

```sh
./r_dev_runner_register.sh <group_token>
```

You can check the runners registered in the **CI/CD** settings of your group.

## Launching your Runners

To launch your runners execute the following command:

```sh
./runner_start.sh
```

To check the GitLab runner status:

```sh
docker ps
```

## Sopping your Runners

To stop your runners, execute the following command:

```sh
./runner_stop.sh
```


## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process of submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.biologie.ens-lyon.fr/pipelines/nextflow/tags). 

## Authors

* **Laurent Modolo** - *Initial work*

See also the list of [contributors](https://gitlab.biologie.ens-lyon.fr/pipelines/nextflow/graphs/master) who participated in this project.

## License

This project is licensed under the CeCiLL License- see the [LICENSE](LICENSE) file for details.

